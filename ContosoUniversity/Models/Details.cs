﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class Details
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DetailsID { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string Tel { get; set; }
        [StringLength(50, MinimumLength = 3)]
        public string Adresse { get; set; }

        public int PersonneID { get; set; }

        public virtual Personne Pesronnes { get; set; }
    }
}