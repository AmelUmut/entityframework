namespace ContosoUniversity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Details",
                c => new
                    {
                        DetailsID = c.Int(nullable: false),
                        Tel = c.String(maxLength: 50),
                        Adresse = c.String(maxLength: 50),
                        PersonneID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DetailsID)
                .ForeignKey("dbo.Personne", t => t.PersonneID, cascadeDelete: true)
                .Index(t => t.PersonneID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Details", "PersonneID", "dbo.Personne");
            DropIndex("dbo.Details", new[] { "PersonneID" });
            DropTable("dbo.Details");
        }
    }
}
